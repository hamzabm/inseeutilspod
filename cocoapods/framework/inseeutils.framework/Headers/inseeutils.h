#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class InseeutilsKotlinError, InseeutilsComparatorManager, InseeutilsCurrencyManager, InseeutilsFirstNamesManager, InseeutilsRentManager, InseeutilsInseeModule, InseeutilsConverterDao, InseeutilsCurrencyUtilsCurrencyResponse, InseeutilsCurrencyUtilsCurrency, InseeutilsCurrencyUtilsCurrencyData, InseeutilsKotlinEnum<E>, InseeutilsKotlinArray<T>, InseeutilsComparator, InseeutilsRemoteDbConfig, InseeutilsPrenomsAdapter, InseeutilsRentDao, InseeutilsGetRentUpdate, InseeutilsTrimester, InseeutilsNamesDao, InseeutilsGetBestRankInYearRange, InseeutilsSexe, InseeutilsRank, InseeutilsGetBestYearsRank, InseeutilsRuntimeQuery<__covariant RowType>, InseeutilsPrenoms, InseeutilsAgeCentenarianCard, InseeutilsAgeData, InseeutilsAgeGenderCard, InseeutilsAgeGenerationCard, InseeutilsAgeSection, InseeutilsCspSection, InseeutilsDegreeSection, InseeutilsIncomeSection, InseeutilsLifeSection, InseeutilsReference, InseeutilsWeddingSection, InseeutilsWorkerSection, InseeutilsCspRow, InseeutilsCspCard, InseeutilsDegreeData, InseeutilsDegreeFemale, InseeutilsDegreeMale, InseeutilsDegreeGenerationCard, InseeutilsDegreeRow, InseeutilsDegreeUserCard, InseeutilsIncomeRange, InseeutilsIncomeDatas, InseeutilsIncomeParityCard, InseeutilsIncomeUserCard, InseeutilsLifeEuropeRow, InseeutilsLifeEuropeCard, InseeutilsLifeFranceRow, InseeutilsLifeFranceCard, InseeutilsReferenceLink, InseeutilsWeddingRow, InseeutilsWeddingAgeCard, InseeutilsWeddingThirtyCard, InseeutilsWorkerEnterpriseCard, InseeutilsWorkerGenderCard, InseeutilsWorkerGenerationBefore, InseeutilsWorkerGenerationNow, InseeutilsWorkerGenerationCard, InseeutilsThematic, InseeutilsConvertisseur, InseeutilsSelectMax, InseeutilsSelectMin, InseeutilsGetAccuranceNumberOnYearRangeById, InseeutilsGetMaxFirstNameYear, InseeutilsGetMaxRentYear, InseeutilsGetMinFirstNameYear, InseeutilsGetMinRentYear, InseeutilsGetRankByCount, InseeutilsLoyer, InseeutilsTop100FirstNames, InseeutilsPrenoms_stats, InseeutilsKotlinThrowable, InseeutilsRuntimeTransacterTransaction, InseeutilsKotlinx_serialization_coreSerializersModule, InseeutilsKotlinx_serialization_coreSerialKind, InseeutilsKotlinNothing, InseeutilsKotlinByteArray, InseeutilsKotlinByteIterator;

@protocol InseeutilsKotlinComparable, InseeutilsKotlinx_serialization_coreKSerializer, InseeutilsConvertiseurModelQueries, InseeutilsRuntimeTransactionWithoutReturn, InseeutilsRuntimeTransactionWithReturn, InseeutilsRuntimeTransacter, InseeutilsConverterDatabase, InseeutilsRuntimeSqlDriver, InseeutilsRuntimeSqlDriverSchema, InseeutilsNamesDatabase, InseeutilsRentDatabase, InseeutilsRentModelQueries, InseeutilsNamesModelQueries, InseeutilsRuntimeColumnAdapter, InseeutilsKotlinIterator, InseeutilsKotlinx_serialization_coreEncoder, InseeutilsKotlinx_serialization_coreSerialDescriptor, InseeutilsKotlinx_serialization_coreSerializationStrategy, InseeutilsKotlinx_serialization_coreDecoder, InseeutilsKotlinx_serialization_coreDeserializationStrategy, InseeutilsRuntimeTransactionCallbacks, InseeutilsRuntimeSqlPreparedStatement, InseeutilsRuntimeSqlCursor, InseeutilsRuntimeCloseable, InseeutilsRuntimeQueryListener, InseeutilsKotlinx_serialization_coreCompositeEncoder, InseeutilsKotlinAnnotation, InseeutilsKotlinx_serialization_coreCompositeDecoder, InseeutilsKotlinx_serialization_coreSerializersModuleCollector, InseeutilsKotlinKClass, InseeutilsKotlinKDeclarationContainer, InseeutilsKotlinKAnnotatedElement, InseeutilsKotlinKClassifier;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

__attribute__((swift_name("KotlinBase")))
@interface InseeutilsBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface InseeutilsBase (InseeutilsBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface InseeutilsMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface InseeutilsMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorInseeutilsKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface InseeutilsNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface InseeutilsByte : InseeutilsNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface InseeutilsUByte : InseeutilsNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface InseeutilsShort : InseeutilsNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface InseeutilsUShort : InseeutilsNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface InseeutilsInt : InseeutilsNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface InseeutilsUInt : InseeutilsNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface InseeutilsLong : InseeutilsNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface InseeutilsULong : InseeutilsNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface InseeutilsFloat : InseeutilsNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface InseeutilsDouble : InseeutilsNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface InseeutilsBoolean : InseeutilsNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InseeModule")))
@interface InseeutilsInseeModule : InseeutilsBase
- (instancetype)initWithContext:(id)context __attribute__((swift_name("init(context:)"))) __attribute__((objc_designated_initializer));
- (void)prepareDataIfNeededOnReady:(void (^)(void))onReady onPrepareFail:(void (^)(InseeutilsKotlinError *))onPrepareFail __attribute__((swift_name("prepareDataIfNeeded(onReady:onPrepareFail:)")));
@property InseeutilsComparatorManager *comparatorManager __attribute__((swift_name("comparatorManager")));
@property (readonly) id context __attribute__((swift_name("context")));
@property InseeutilsCurrencyManager *currencyManager __attribute__((swift_name("currencyManager")));
@property InseeutilsFirstNamesManager *firstNamesManager __attribute__((swift_name("firstNamesManager")));
@property InseeutilsRentManager *rentManager __attribute__((swift_name("rentManager")));
@property InseeutilsInseeModule *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CurrencyManager")))
@interface InseeutilsCurrencyManager : InseeutilsBase
- (instancetype)initWithConverterDao:(InseeutilsConverterDao *)converterDao __attribute__((swift_name("init(converterDao:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsCurrencyUtilsCurrencyResponse *)convertCurrencyHowMany:(float)howMany fromYear:(int64_t)fromYear toYear:(int64_t)toYear __attribute__((swift_name("convertCurrency(howMany:fromYear:toYear:)")));
- (InseeutilsCurrencyUtilsCurrency *)getCurrencyFromYearYear:(int64_t)year __attribute__((swift_name("getCurrencyFromYear(year:)")));
- (int64_t)getMaxYear __attribute__((swift_name("getMaxYear()")));
- (int64_t)getMinYear __attribute__((swift_name("getMinYear()")));
- (NSArray<InseeutilsLong *> *)getYearList __attribute__((swift_name("getYearList()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CurrencyUtils")))
@interface InseeutilsCurrencyUtils : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)currencyUtils __attribute__((swift_name("init()")));
- (float)currencyConverterWithDecimalHowMany:(float)howMany from:(InseeutilsCurrencyUtilsCurrencyData *)from to:(InseeutilsCurrencyUtilsCurrencyData *)to dicimal:(int32_t)dicimal __attribute__((swift_name("currencyConverterWithDecimal(howMany:from:to:dicimal:)")));
- (InseeutilsCurrencyUtilsCurrency *)getCurrencyByYearYear:(int64_t)year __attribute__((swift_name("getCurrencyByYear(year:)")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol InseeutilsKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface InseeutilsKotlinEnum<E> : InseeutilsBase <InseeutilsKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CurrencyUtils.Currency")))
@interface InseeutilsCurrencyUtilsCurrency : InseeutilsKotlinEnum<InseeutilsCurrencyUtilsCurrency *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) InseeutilsCurrencyUtilsCurrency *euro __attribute__((swift_name("euro")));
@property (class, readonly) InseeutilsCurrencyUtilsCurrency *franc __attribute__((swift_name("franc")));
@property (class, readonly) InseeutilsCurrencyUtilsCurrency *oldFranc __attribute__((swift_name("oldFranc")));
@property (class, readonly) InseeutilsCurrencyUtilsCurrency *error __attribute__((swift_name("error")));
+ (InseeutilsKotlinArray<InseeutilsCurrencyUtilsCurrency *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CurrencyUtils.CurrencyData")))
@interface InseeutilsCurrencyUtilsCurrencyData : InseeutilsBase
- (instancetype)initWithCurrency:(InseeutilsCurrencyUtilsCurrency *)currency indexValue:(float)indexValue __attribute__((swift_name("init(currency:indexValue:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsCurrencyUtilsCurrency *)component1 __attribute__((swift_name("component1()")));
- (float)component2 __attribute__((swift_name("component2()")));
- (InseeutilsCurrencyUtilsCurrencyData *)doCopyCurrency:(InseeutilsCurrencyUtilsCurrency *)currency indexValue:(float)indexValue __attribute__((swift_name("doCopy(currency:indexValue:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsCurrencyUtilsCurrency *currency __attribute__((swift_name("currency")));
@property (readonly) float indexValue __attribute__((swift_name("indexValue")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CurrencyUtils.CurrencyResponse")))
@interface InseeutilsCurrencyUtilsCurrencyResponse : InseeutilsBase
- (instancetype)initWithFromCurrency:(InseeutilsCurrencyUtilsCurrency *)fromCurrency toCurrency:(InseeutilsCurrencyUtilsCurrency *)toCurrency valueToConvert:(float)valueToConvert valueConverted:(float)valueConverted __attribute__((swift_name("init(fromCurrency:toCurrency:valueToConvert:valueConverted:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsCurrencyUtilsCurrency *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsCurrencyUtilsCurrency *)component2 __attribute__((swift_name("component2()")));
- (float)component3 __attribute__((swift_name("component3()")));
- (float)component4 __attribute__((swift_name("component4()")));
- (InseeutilsCurrencyUtilsCurrencyResponse *)doCopyFromCurrency:(InseeutilsCurrencyUtilsCurrency *)fromCurrency toCurrency:(InseeutilsCurrencyUtilsCurrency *)toCurrency valueToConvert:(float)valueToConvert valueConverted:(float)valueConverted __attribute__((swift_name("doCopy(fromCurrency:toCurrency:valueToConvert:valueConverted:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsCurrencyUtilsCurrency *fromCurrency __attribute__((swift_name("fromCurrency")));
@property (readonly) InseeutilsCurrencyUtilsCurrency *toCurrency __attribute__((swift_name("toCurrency")));
@property (readonly) float valueConverted __attribute__((swift_name("valueConverted")));
@property (readonly) float valueToConvert __attribute__((swift_name("valueToConvert")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirebaseRepo")))
@interface InseeutilsFirebaseRepo : InseeutilsBase
- (instancetype)initWithContext:(id _Nullable)context __attribute__((swift_name("init(context:)"))) __attribute__((objc_designated_initializer));
- (void)getComparatorType:(NSString *)type onResult:(void (^)(InseeutilsComparator *))onResult onFail:(void (^)(InseeutilsKotlinError *))onFail __attribute__((swift_name("getComparator(type:onResult:onFail:)")));
- (void)getDbFromStorageUrl:(NSString *)url createdAt:(NSString *)createdAt type:(NSString *)type dbName:(NSString *)dbName success:(void (^)(NSString *))success fail:(void (^)(InseeutilsKotlinError *))fail __attribute__((swift_name("getDbFromStorage(url:createdAt:type:dbName:success:fail:)")));
- (void)getUrlFromRemoteConfigAndCheckIfNeedToDownloadType:(NSString *)type dbName:(NSString *)dbName success:(void (^)(InseeutilsBoolean *, NSString *, NSString *))success fail:(void (^)(InseeutilsKotlinError *))fail __attribute__((swift_name("getUrlFromRemoteConfigAndCheckIfNeedToDownload(type:dbName:success:fail:)")));
- (BOOL)needToUpdateDbType:(NSString *)type createdAt:(NSString *)createdAt __attribute__((swift_name("needToUpdateDb(type:createdAt:)")));
- (void)updateIndexFolderType:(NSString *)type createdAt:(NSString *)createdAt __attribute__((swift_name("updateIndexFolder(type:createdAt:)")));
@property (readonly) id _Nullable context __attribute__((swift_name("context")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InseeFileType")))
@interface InseeutilsInseeFileType : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)inseeFileType __attribute__((swift_name("init()")));
@property (readonly) NSString *CHILD_SUPPORT __attribute__((swift_name("CHILD_SUPPORT")));
@property (readonly) NSString *COMPARATOR __attribute__((swift_name("COMPARATOR")));
@property (readonly) NSString *EURO_CONVERTER __attribute__((swift_name("EURO_CONVERTER")));
@property (readonly) NSString *FIRSTNAMES __attribute__((swift_name("FIRSTNAMES")));
@property (readonly) NSString *RENT_REVIEW __attribute__((swift_name("RENT_REVIEW")));
@end;

__attribute__((swift_name("OnResultResponse")))
@protocol InseeutilsOnResultResponse
@required
- (void)failErr:(InseeutilsKotlinError *)err __attribute__((swift_name("fail(err:)")));
- (void)successPath:(NSString *)path __attribute__((swift_name("success(path:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RemoteDbConfig")))
@interface InseeutilsRemoteDbConfig : InseeutilsBase
- (instancetype)initWithFile_path:(NSString *)file_path updated_at:(double)updated_at __attribute__((swift_name("init(file_path:updated_at:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (double)component2 __attribute__((swift_name("component2()")));
- (InseeutilsRemoteDbConfig *)doCopyFile_path:(NSString *)file_path updated_at:(double)updated_at __attribute__((swift_name("doCopy(file_path:updated_at:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *file_path __attribute__((swift_name("file_path")));
@property (readonly) double updated_at __attribute__((swift_name("updated_at")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RemoteDbConfig.Companion")))
@interface InseeutilsRemoteDbConfigCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("RuntimeTransacter")))
@protocol InseeutilsRuntimeTransacter
@required
- (void)transactionNoEnclosing:(BOOL)noEnclosing body:(void (^)(id<InseeutilsRuntimeTransactionWithoutReturn>))body __attribute__((swift_name("transaction(noEnclosing:body:)")));
- (id _Nullable)transactionWithResultNoEnclosing:(BOOL)noEnclosing bodyWithReturn:(id _Nullable (^)(id<InseeutilsRuntimeTransactionWithReturn>))bodyWithReturn __attribute__((swift_name("transactionWithResult(noEnclosing:bodyWithReturn:)")));
@end;

__attribute__((swift_name("ConverterDatabase")))
@protocol InseeutilsConverterDatabase <InseeutilsRuntimeTransacter>
@required
@property (readonly) id<InseeutilsConvertiseurModelQueries> convertiseurModelQueries __attribute__((swift_name("convertiseurModelQueries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConverterDatabaseCompanion")))
@interface InseeutilsConverterDatabaseCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsConverterDatabase>)invokeDriver:(id<InseeutilsRuntimeSqlDriver>)driver __attribute__((swift_name("invoke(driver:)")));
@property (readonly) id<InseeutilsRuntimeSqlDriverSchema> Schema __attribute__((swift_name("Schema")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DriverFactory")))
@interface InseeutilsDriverFactory : InseeutilsBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<InseeutilsRuntimeSqlDriver>)createDriverContext:(id _Nullable)context schema:(id<InseeutilsRuntimeSqlDriverSchema>)schema dbName:(NSString *)dbName __attribute__((swift_name("createDriver(context:schema:dbName:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConverterDao")))
@interface InseeutilsConverterDao : InseeutilsBase
- (instancetype)initWithDatabase:(id<InseeutilsConverterDatabase>)database __attribute__((swift_name("init(database:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NamesDao")))
@interface InseeutilsNamesDao : InseeutilsBase
- (instancetype)initWithDatabase:(id<InseeutilsNamesDatabase>)database __attribute__((swift_name("init(database:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RentDao")))
@interface InseeutilsRentDao : InseeutilsBase
- (instancetype)initWithDatabase:(id<InseeutilsRentDatabase>)database __attribute__((swift_name("init(database:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("RentDatabase")))
@protocol InseeutilsRentDatabase <InseeutilsRuntimeTransacter>
@required
@property (readonly) id<InseeutilsRentModelQueries> rentModelQueries __attribute__((swift_name("rentModelQueries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RentDatabaseCompanion")))
@interface InseeutilsRentDatabaseCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsRentDatabase>)invokeDriver:(id<InseeutilsRuntimeSqlDriver>)driver __attribute__((swift_name("invoke(driver:)")));
@property (readonly) id<InseeutilsRuntimeSqlDriverSchema> Schema __attribute__((swift_name("Schema")));
@end;

__attribute__((swift_name("NamesDatabase")))
@protocol InseeutilsNamesDatabase <InseeutilsRuntimeTransacter>
@required
@property (readonly) id<InseeutilsNamesModelQueries> namesModelQueries __attribute__((swift_name("namesModelQueries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NamesDatabaseCompanion")))
@interface InseeutilsNamesDatabaseCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsNamesDatabase>)invokeDriver:(id<InseeutilsRuntimeSqlDriver>)driver prenomsAdapter:(InseeutilsPrenomsAdapter *)prenomsAdapter __attribute__((swift_name("invoke(driver:prenomsAdapter:)")));
@property (readonly) id<InseeutilsRuntimeSqlDriverSchema> Schema __attribute__((swift_name("Schema")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RentManager")))
@interface InseeutilsRentManager : InseeutilsBase
- (instancetype)initWithRentDao:(InseeutilsRentDao *)rentDao __attribute__((swift_name("init(rentDao:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)getMaxYear __attribute__((swift_name("getMaxYear()")));
- (InseeutilsLong * _Nullable)getMinYear __attribute__((swift_name("getMinYear()")));
- (NSArray<InseeutilsLong *> *)getYearList __attribute__((swift_name("getYearList()")));
- (NSArray<InseeutilsGetRentUpdate *> *)rentReviewRent:(double)rent trimester:(InseeutilsTrimester *)trimester bailDate:(NSString *)bailDate __attribute__((swift_name("rentReview(rent:trimester:bailDate:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Trimester")))
@interface InseeutilsTrimester : InseeutilsKotlinEnum<InseeutilsTrimester *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) InseeutilsTrimester *t1 __attribute__((swift_name("t1")));
@property (class, readonly) InseeutilsTrimester *t2 __attribute__((swift_name("t2")));
@property (class, readonly) InseeutilsTrimester *t3 __attribute__((swift_name("t3")));
@property (class, readonly) InseeutilsTrimester *t4 __attribute__((swift_name("t4")));
+ (InseeutilsKotlinArray<InseeutilsTrimester *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstNamesManager")))
@interface InseeutilsFirstNamesManager : InseeutilsBase
- (instancetype)initWithNamesDao:(InseeutilsNamesDao *)namesDao __attribute__((swift_name("init(namesDao:)"))) __attribute__((objc_designated_initializer));
- (NSArray<InseeutilsGetBestRankInYearRange *> *)get100BestRankInYearRangeFromYear:(int64_t)fromYear toYear:(int64_t)toYear sexe:(InseeutilsSexe *)sexe __attribute__((swift_name("get100BestRankInYearRange(fromYear:toYear:sexe:)")));
- (InseeutilsRank *)getBestRankInYearRangeOfOneFirstNameFromYear:(int64_t)fromYear toYear:(int64_t)toYear prenomId:(int64_t)prenomId __attribute__((swift_name("getBestRankInYearRangeOfOneFirstName(fromYear:toYear:prenomId:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetBestYearsRank *> *)getBestYearsRankOfOneFirstNamePrenomId:(int64_t)prenomId __attribute__((swift_name("getBestYearsRankOfOneFirstName(prenomId:)")));
- (InseeutilsLong * _Nullable)getMaxYear __attribute__((swift_name("getMaxYear()")));
- (InseeutilsLong * _Nullable)getMinYear __attribute__((swift_name("getMinYear()")));
- (NSArray<InseeutilsLong *> *)getYearList __attribute__((swift_name("getYearList()")));
- (NSArray<InseeutilsPrenoms *> *)searchForFirstNameSearchFor:(NSString *)searchFor __attribute__((swift_name("searchForFirstName(searchFor:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Rank")))
@interface InseeutilsRank : InseeutilsBase
- (instancetype)initWithPrenom:(InseeutilsPrenoms *)prenom cont:(InseeutilsLong * _Nullable)cont rank:(int32_t)rank __attribute__((swift_name("init(prenom:cont:rank:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsPrenoms *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsLong * _Nullable)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (InseeutilsRank *)doCopyPrenom:(InseeutilsPrenoms *)prenom cont:(InseeutilsLong * _Nullable)cont rank:(int32_t)rank __attribute__((swift_name("doCopy(prenom:cont:rank:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable cont __attribute__((swift_name("cont")));
@property (readonly) InseeutilsPrenoms *prenom __attribute__((swift_name("prenom")));
@property (readonly) int32_t rank __attribute__((swift_name("rank")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ComparatorManager")))
@interface InseeutilsComparatorManager : InseeutilsBase
- (instancetype)initWithComparator:(InseeutilsComparator *)comparator __attribute__((swift_name("init(comparator:)"))) __attribute__((objc_designated_initializer));
@property (readonly) InseeutilsComparator *comparator __attribute__((swift_name("comparator")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeCentenarianCard")))
@interface InseeutilsAgeCentenarianCard : InseeutilsBase
- (instancetype)initWithFemale_subtitle:(NSString *)female_subtitle female_title:(NSString *)female_title figure:(NSString *)figure legend:(NSString *)legend male_subtitle:(NSString *)male_subtitle male_title:(NSString *)male_title separator:(NSString *)separator __attribute__((swift_name("init(female_subtitle:female_title:figure:legend:male_subtitle:male_title:separator:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString *)component7 __attribute__((swift_name("component7()")));
- (InseeutilsAgeCentenarianCard *)doCopyFemale_subtitle:(NSString *)female_subtitle female_title:(NSString *)female_title figure:(NSString *)figure legend:(NSString *)legend male_subtitle:(NSString *)male_subtitle male_title:(NSString *)male_title separator:(NSString *)separator __attribute__((swift_name("doCopy(female_subtitle:female_title:figure:legend:male_subtitle:male_title:separator:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *female_subtitle __attribute__((swift_name("female_subtitle")));
@property (readonly) NSString *female_title __attribute__((swift_name("female_title")));
@property (readonly) NSString *figure __attribute__((swift_name("figure")));
@property (readonly) NSString *legend __attribute__((swift_name("legend")));
@property (readonly) NSString *male_subtitle __attribute__((swift_name("male_subtitle")));
@property (readonly) NSString *male_title __attribute__((swift_name("male_title")));
@property (readonly) NSString *separator __attribute__((swift_name("separator")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeCentenarianCard.Companion")))
@interface InseeutilsAgeCentenarianCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeData")))
@interface InseeutilsAgeData : InseeutilsBase
- (instancetype)initWithFemale_count:(NSString *)female_count generation_name:(NSString *)generation_name generation_percent:(NSString *)generation_percent male_count:(NSString *)male_count year:(int32_t)year __attribute__((swift_name("init(female_count:generation_name:generation_percent:male_count:year:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (InseeutilsAgeData *)doCopyFemale_count:(NSString *)female_count generation_name:(NSString *)generation_name generation_percent:(NSString *)generation_percent male_count:(NSString *)male_count year:(int32_t)year __attribute__((swift_name("doCopy(female_count:generation_name:generation_percent:male_count:year:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *female_count __attribute__((swift_name("female_count")));
@property (readonly) NSString *generation_name __attribute__((swift_name("generation_name")));
@property (readonly) NSString *generation_percent __attribute__((swift_name("generation_percent")));
@property (readonly) NSString *male_count __attribute__((swift_name("male_count")));
@property (readonly) int32_t year __attribute__((swift_name("year")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeData.Companion")))
@interface InseeutilsAgeDataCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeGenderCard")))
@interface InseeutilsAgeGenderCard : InseeutilsBase
- (instancetype)initWithFemale_subtitle:(NSString *)female_subtitle female_title:(NSString *)female_title male_subtitle:(NSString *)male_subtitle male_title:(NSString *)male_title __attribute__((swift_name("init(female_subtitle:female_title:male_subtitle:male_title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (InseeutilsAgeGenderCard *)doCopyFemale_subtitle:(NSString *)female_subtitle female_title:(NSString *)female_title male_subtitle:(NSString *)male_subtitle male_title:(NSString *)male_title __attribute__((swift_name("doCopy(female_subtitle:female_title:male_subtitle:male_title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *female_subtitle __attribute__((swift_name("female_subtitle")));
@property (readonly) NSString *female_title __attribute__((swift_name("female_title")));
@property (readonly) NSString *male_subtitle __attribute__((swift_name("male_subtitle")));
@property (readonly) NSString *male_title __attribute__((swift_name("male_title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeGenderCard.Companion")))
@interface InseeutilsAgeGenderCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeGenerationCard")))
@interface InseeutilsAgeGenerationCard : InseeutilsBase
- (instancetype)initWithFooter:(NSString *)footer header:(NSString *)header separator:(NSString *)separator __attribute__((swift_name("init(footer:header:separator:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsAgeGenerationCard *)doCopyFooter:(NSString *)footer header:(NSString *)header separator:(NSString *)separator __attribute__((swift_name("doCopy(footer:header:separator:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *footer __attribute__((swift_name("footer")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@property (readonly) NSString *separator __attribute__((swift_name("separator")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeGenerationCard.Companion")))
@interface InseeutilsAgeGenerationCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeSection")))
@interface InseeutilsAgeSection : InseeutilsBase
- (instancetype)initWithAge_centenarian_card:(InseeutilsAgeCentenarianCard *)age_centenarian_card age_data:(NSArray<InseeutilsAgeData *> *)age_data age_gender_card:(InseeutilsAgeGenderCard *)age_gender_card age_generation_card:(InseeutilsAgeGenerationCard *)age_generation_card section_title:(NSString *)section_title __attribute__((swift_name("init(age_centenarian_card:age_data:age_gender_card:age_generation_card:section_title:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsAgeCentenarianCard *)component1 __attribute__((swift_name("component1()")));
- (NSArray<InseeutilsAgeData *> *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsAgeGenderCard *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsAgeGenerationCard *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (InseeutilsAgeSection *)doCopyAge_centenarian_card:(InseeutilsAgeCentenarianCard *)age_centenarian_card age_data:(NSArray<InseeutilsAgeData *> *)age_data age_gender_card:(InseeutilsAgeGenderCard *)age_gender_card age_generation_card:(InseeutilsAgeGenerationCard *)age_generation_card section_title:(NSString *)section_title __attribute__((swift_name("doCopy(age_centenarian_card:age_data:age_gender_card:age_generation_card:section_title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsAgeCentenarianCard *age_centenarian_card __attribute__((swift_name("age_centenarian_card")));
@property (readonly) NSArray<InseeutilsAgeData *> *age_data __attribute__((swift_name("age_data")));
@property (readonly) InseeutilsAgeGenderCard *age_gender_card __attribute__((swift_name("age_gender_card")));
@property (readonly) InseeutilsAgeGenerationCard *age_generation_card __attribute__((swift_name("age_generation_card")));
@property (readonly) NSString *section_title __attribute__((swift_name("section_title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AgeSection.Companion")))
@interface InseeutilsAgeSectionCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Comparator")))
@interface InseeutilsComparator : InseeutilsBase
- (instancetype)initWithAge_section:(InseeutilsAgeSection *)age_section csp_section:(InseeutilsCspSection *)csp_section degree_section:(InseeutilsDegreeSection *)degree_section income_section:(InseeutilsIncomeSection *)income_section life_section:(InseeutilsLifeSection *)life_section references:(NSArray<InseeutilsReference *> *)references wedding_section:(InseeutilsWeddingSection *)wedding_section worker_section:(InseeutilsWorkerSection *)worker_section __attribute__((swift_name("init(age_section:csp_section:degree_section:income_section:life_section:references:wedding_section:worker_section:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsAgeSection *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsCspSection *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsDegreeSection *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsIncomeSection *)component4 __attribute__((swift_name("component4()")));
- (InseeutilsLifeSection *)component5 __attribute__((swift_name("component5()")));
- (NSArray<InseeutilsReference *> *)component6 __attribute__((swift_name("component6()")));
- (InseeutilsWeddingSection *)component7 __attribute__((swift_name("component7()")));
- (InseeutilsWorkerSection *)component8 __attribute__((swift_name("component8()")));
- (InseeutilsComparator *)doCopyAge_section:(InseeutilsAgeSection *)age_section csp_section:(InseeutilsCspSection *)csp_section degree_section:(InseeutilsDegreeSection *)degree_section income_section:(InseeutilsIncomeSection *)income_section life_section:(InseeutilsLifeSection *)life_section references:(NSArray<InseeutilsReference *> *)references wedding_section:(InseeutilsWeddingSection *)wedding_section worker_section:(InseeutilsWorkerSection *)worker_section __attribute__((swift_name("doCopy(age_section:csp_section:degree_section:income_section:life_section:references:wedding_section:worker_section:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsAgeSection *age_section __attribute__((swift_name("age_section")));
@property (readonly) InseeutilsCspSection *csp_section __attribute__((swift_name("csp_section")));
@property (readonly) InseeutilsDegreeSection *degree_section __attribute__((swift_name("degree_section")));
@property (readonly) InseeutilsIncomeSection *income_section __attribute__((swift_name("income_section")));
@property (readonly) InseeutilsLifeSection *life_section __attribute__((swift_name("life_section")));
@property (readonly) NSArray<InseeutilsReference *> *references __attribute__((swift_name("references")));
@property (readonly) InseeutilsWeddingSection *wedding_section __attribute__((swift_name("wedding_section")));
@property (readonly) InseeutilsWorkerSection *worker_section __attribute__((swift_name("worker_section")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Comparator.Companion")))
@interface InseeutilsComparatorCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CspCard")))
@interface InseeutilsCspCard : InseeutilsBase
- (instancetype)initWithCsp_rows:(NSArray<InseeutilsCspRow *> *)csp_rows header:(NSString *)header __attribute__((swift_name("init(csp_rows:header:)"))) __attribute__((objc_designated_initializer));
- (NSArray<InseeutilsCspRow *> *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsCspCard *)doCopyCsp_rows:(NSArray<InseeutilsCspRow *> *)csp_rows header:(NSString *)header __attribute__((swift_name("doCopy(csp_rows:header:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<InseeutilsCspRow *> *csp_rows __attribute__((swift_name("csp_rows")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CspCard.Companion")))
@interface InseeutilsCspCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CspRow")))
@interface InseeutilsCspRow : InseeutilsBase
- (instancetype)initWithHighlighted:(NSString *)highlighted subtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(highlighted:subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsCspRow *)doCopyHighlighted:(NSString *)highlighted subtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(highlighted:subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *highlighted __attribute__((swift_name("highlighted")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CspRow.Companion")))
@interface InseeutilsCspRowCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CspSection")))
@interface InseeutilsCspSection : InseeutilsBase
- (instancetype)initWithCsp_card:(InseeutilsCspCard *)csp_card section_title:(NSString *)section_title __attribute__((swift_name("init(csp_card:section_title:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsCspCard *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsCspSection *)doCopyCsp_card:(InseeutilsCspCard *)csp_card section_title:(NSString *)section_title __attribute__((swift_name("doCopy(csp_card:section_title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsCspCard *csp_card __attribute__((swift_name("csp_card")));
@property (readonly) NSString *section_title __attribute__((swift_name("section_title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CspSection.Companion")))
@interface InseeutilsCspSectionCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeData")))
@interface InseeutilsDegreeData : InseeutilsBase
- (instancetype)initWithComparator_title:(NSString *)comparator_title degree:(NSString *)degree percent:(NSString *)percent profile_title:(NSString *)profile_title __attribute__((swift_name("init(comparator_title:degree:percent:profile_title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (InseeutilsDegreeData *)doCopyComparator_title:(NSString *)comparator_title degree:(NSString *)degree percent:(NSString *)percent profile_title:(NSString *)profile_title __attribute__((swift_name("doCopy(comparator_title:degree:percent:profile_title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *comparator_title __attribute__((swift_name("comparator_title")));
@property (readonly) NSString *degree __attribute__((swift_name("degree")));
@property (readonly) NSString *percent __attribute__((swift_name("percent")));
@property (readonly) NSString *profile_title __attribute__((swift_name("profile_title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeData.Companion")))
@interface InseeutilsDegreeDataCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeFemale")))
@interface InseeutilsDegreeFemale : InseeutilsBase
- (instancetype)initWithSubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsDegreeFemale *)doCopySubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeFemale.Companion")))
@interface InseeutilsDegreeFemaleCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeGenerationCard")))
@interface InseeutilsDegreeGenerationCard : InseeutilsBase
- (instancetype)initWithDegree_female:(InseeutilsDegreeFemale *)degree_female degree_male:(InseeutilsDegreeMale *)degree_male footer:(NSString *)footer header:(NSString *)header __attribute__((swift_name("init(degree_female:degree_male:footer:header:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsDegreeFemale *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsDegreeMale *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (InseeutilsDegreeGenerationCard *)doCopyDegree_female:(InseeutilsDegreeFemale *)degree_female degree_male:(InseeutilsDegreeMale *)degree_male footer:(NSString *)footer header:(NSString *)header __attribute__((swift_name("doCopy(degree_female:degree_male:footer:header:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsDegreeFemale *degree_female __attribute__((swift_name("degree_female")));
@property (readonly) InseeutilsDegreeMale *degree_male __attribute__((swift_name("degree_male")));
@property (readonly) NSString *footer __attribute__((swift_name("footer")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeGenerationCard.Companion")))
@interface InseeutilsDegreeGenerationCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeMale")))
@interface InseeutilsDegreeMale : InseeutilsBase
- (instancetype)initWithSubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsDegreeMale *)doCopySubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeMale.Companion")))
@interface InseeutilsDegreeMaleCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeRow")))
@interface InseeutilsDegreeRow : InseeutilsBase
- (instancetype)initWithFigure:(NSString *)figure legend:(NSString *)legend __attribute__((swift_name("init(figure:legend:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsDegreeRow *)doCopyFigure:(NSString *)figure legend:(NSString *)legend __attribute__((swift_name("doCopy(figure:legend:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *figure __attribute__((swift_name("figure")));
@property (readonly) NSString *legend __attribute__((swift_name("legend")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeRow.Companion")))
@interface InseeutilsDegreeRowCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeSection")))
@interface InseeutilsDegreeSection : InseeutilsBase
- (instancetype)initWithDegree_generation_card:(InseeutilsDegreeGenerationCard *)degree_generation_card degree_user_card:(InseeutilsDegreeUserCard *)degree_user_card section_title:(NSString *)section_title __attribute__((swift_name("init(degree_generation_card:degree_user_card:section_title:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsDegreeGenerationCard *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsDegreeUserCard *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsDegreeSection *)doCopyDegree_generation_card:(InseeutilsDegreeGenerationCard *)degree_generation_card degree_user_card:(InseeutilsDegreeUserCard *)degree_user_card section_title:(NSString *)section_title __attribute__((swift_name("doCopy(degree_generation_card:degree_user_card:section_title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsDegreeGenerationCard *degree_generation_card __attribute__((swift_name("degree_generation_card")));
@property (readonly) InseeutilsDegreeUserCard *degree_user_card __attribute__((swift_name("degree_user_card")));
@property (readonly) NSString *section_title __attribute__((swift_name("section_title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeSection.Companion")))
@interface InseeutilsDegreeSectionCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeUserCard")))
@interface InseeutilsDegreeUserCard : InseeutilsBase
- (instancetype)initWithDegree_datas:(NSArray<InseeutilsDegreeData *> *)degree_datas degree_rows:(NSArray<InseeutilsDegreeRow *> *)degree_rows footer:(NSString *)footer header_1:(NSString *)header_1 header_2:(NSString *)header_2 title:(NSString *)title __attribute__((swift_name("init(degree_datas:degree_rows:footer:header_1:header_2:title:)"))) __attribute__((objc_designated_initializer));
- (NSArray<InseeutilsDegreeData *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<InseeutilsDegreeRow *> *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (InseeutilsDegreeUserCard *)doCopyDegree_datas:(NSArray<InseeutilsDegreeData *> *)degree_datas degree_rows:(NSArray<InseeutilsDegreeRow *> *)degree_rows footer:(NSString *)footer header_1:(NSString *)header_1 header_2:(NSString *)header_2 title:(NSString *)title __attribute__((swift_name("doCopy(degree_datas:degree_rows:footer:header_1:header_2:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<InseeutilsDegreeData *> *degree_datas __attribute__((swift_name("degree_datas")));
@property (readonly) NSArray<InseeutilsDegreeRow *> *degree_rows __attribute__((swift_name("degree_rows")));
@property (readonly) NSString *footer __attribute__((swift_name("footer")));
@property (readonly) NSString *header_1 __attribute__((swift_name("header_1")));
@property (readonly) NSString *header_2 __attribute__((swift_name("header_2")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DegreeUserCard.Companion")))
@interface InseeutilsDegreeUserCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeDatas")))
@interface InseeutilsIncomeDatas : InseeutilsBase
- (instancetype)initWithFemale:(int32_t)female income_ranges:(NSArray<InseeutilsIncomeRange *> *)income_ranges male:(int32_t)male __attribute__((swift_name("init(female:income_ranges:male:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSArray<InseeutilsIncomeRange *> *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (InseeutilsIncomeDatas *)doCopyFemale:(int32_t)female income_ranges:(NSArray<InseeutilsIncomeRange *> *)income_ranges male:(int32_t)male __attribute__((swift_name("doCopy(female:income_ranges:male:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t female __attribute__((swift_name("female")));
@property (readonly) NSArray<InseeutilsIncomeRange *> *income_ranges __attribute__((swift_name("income_ranges")));
@property (readonly) int32_t male __attribute__((swift_name("male")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeDatas.Companion")))
@interface InseeutilsIncomeDatasCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeParityCard")))
@interface InseeutilsIncomeParityCard : InseeutilsBase
- (instancetype)initWithSubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsIncomeParityCard *)doCopySubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeParityCard.Companion")))
@interface InseeutilsIncomeParityCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeRange")))
@interface InseeutilsIncomeRange : InseeutilsBase
- (instancetype)initWithLower_bound:(int32_t)lower_bound text:(NSString *)text __attribute__((swift_name("init(lower_bound:text:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsIncomeRange *)doCopyLower_bound:(int32_t)lower_bound text:(NSString *)text __attribute__((swift_name("doCopy(lower_bound:text:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t lower_bound __attribute__((swift_name("lower_bound")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeRange.Companion")))
@interface InseeutilsIncomeRangeCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeSection")))
@interface InseeutilsIncomeSection : InseeutilsBase
- (instancetype)initWithIncome_parity_card:(InseeutilsIncomeParityCard *)income_parity_card income_user_card:(InseeutilsIncomeUserCard *)income_user_card section_title:(NSString *)section_title __attribute__((swift_name("init(income_parity_card:income_user_card:section_title:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsIncomeParityCard *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsIncomeUserCard *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsIncomeSection *)doCopyIncome_parity_card:(InseeutilsIncomeParityCard *)income_parity_card income_user_card:(InseeutilsIncomeUserCard *)income_user_card section_title:(NSString *)section_title __attribute__((swift_name("doCopy(income_parity_card:income_user_card:section_title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsIncomeParityCard *income_parity_card __attribute__((swift_name("income_parity_card")));
@property (readonly) InseeutilsIncomeUserCard *income_user_card __attribute__((swift_name("income_user_card")));
@property (readonly) NSString *section_title __attribute__((swift_name("section_title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeSection.Companion")))
@interface InseeutilsIncomeSectionCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeUserCard")))
@interface InseeutilsIncomeUserCard : InseeutilsBase
- (instancetype)initWithFemale_subtitle:(NSString *)female_subtitle footer:(NSString *)footer header_1:(NSString *)header_1 header_2:(NSString *)header_2 income_datas:(InseeutilsIncomeDatas *)income_datas male_subtitle:(NSString *)male_subtitle separator:(NSString *)separator __attribute__((swift_name("init(female_subtitle:footer:header_1:header_2:income_datas:male_subtitle:separator:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (InseeutilsIncomeDatas *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString *)component7 __attribute__((swift_name("component7()")));
- (InseeutilsIncomeUserCard *)doCopyFemale_subtitle:(NSString *)female_subtitle footer:(NSString *)footer header_1:(NSString *)header_1 header_2:(NSString *)header_2 income_datas:(InseeutilsIncomeDatas *)income_datas male_subtitle:(NSString *)male_subtitle separator:(NSString *)separator __attribute__((swift_name("doCopy(female_subtitle:footer:header_1:header_2:income_datas:male_subtitle:separator:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *female_subtitle __attribute__((swift_name("female_subtitle")));
@property (readonly) NSString *footer __attribute__((swift_name("footer")));
@property (readonly) NSString *header_1 __attribute__((swift_name("header_1")));
@property (readonly) NSString *header_2 __attribute__((swift_name("header_2")));
@property (readonly) InseeutilsIncomeDatas *income_datas __attribute__((swift_name("income_datas")));
@property (readonly) NSString *male_subtitle __attribute__((swift_name("male_subtitle")));
@property (readonly) NSString *separator __attribute__((swift_name("separator")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IncomeUserCard.Companion")))
@interface InseeutilsIncomeUserCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeEuropeCard")))
@interface InseeutilsLifeEuropeCard : InseeutilsBase
- (instancetype)initWithHeader:(NSString *)header life_europe_rows:(NSArray<InseeutilsLifeEuropeRow *> *)life_europe_rows __attribute__((swift_name("init(header:life_europe_rows:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSArray<InseeutilsLifeEuropeRow *> *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsLifeEuropeCard *)doCopyHeader:(NSString *)header life_europe_rows:(NSArray<InseeutilsLifeEuropeRow *> *)life_europe_rows __attribute__((swift_name("doCopy(header:life_europe_rows:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@property (readonly) NSArray<InseeutilsLifeEuropeRow *> *life_europe_rows __attribute__((swift_name("life_europe_rows")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeEuropeCard.Companion")))
@interface InseeutilsLifeEuropeCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeEuropeRow")))
@interface InseeutilsLifeEuropeRow : InseeutilsBase
- (instancetype)initWithExponent:(NSString *)exponent position:(NSString *)position subtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(exponent:position:subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (InseeutilsLifeEuropeRow *)doCopyExponent:(NSString *)exponent position:(NSString *)position subtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(exponent:position:subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *exponent __attribute__((swift_name("exponent")));
@property (readonly) NSString *position __attribute__((swift_name("position")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeEuropeRow.Companion")))
@interface InseeutilsLifeEuropeRowCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeFranceCard")))
@interface InseeutilsLifeFranceCard : InseeutilsBase
- (instancetype)initWithHeader:(NSString *)header life_france_rows:(NSArray<InseeutilsLifeFranceRow *> *)life_france_rows __attribute__((swift_name("init(header:life_france_rows:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSArray<InseeutilsLifeFranceRow *> *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsLifeFranceCard *)doCopyHeader:(NSString *)header life_france_rows:(NSArray<InseeutilsLifeFranceRow *> *)life_france_rows __attribute__((swift_name("doCopy(header:life_france_rows:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@property (readonly) NSArray<InseeutilsLifeFranceRow *> *life_france_rows __attribute__((swift_name("life_france_rows")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeFranceCard.Companion")))
@interface InseeutilsLifeFranceCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeFranceRow")))
@interface InseeutilsLifeFranceRow : InseeutilsBase
- (instancetype)initWithFemale:(NSString *)female male:(NSString *)male separator:(NSString *)separator __attribute__((swift_name("init(female:male:separator:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsLifeFranceRow *)doCopyFemale:(NSString *)female male:(NSString *)male separator:(NSString *)separator __attribute__((swift_name("doCopy(female:male:separator:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *female __attribute__((swift_name("female")));
@property (readonly) NSString *male __attribute__((swift_name("male")));
@property (readonly) NSString *separator __attribute__((swift_name("separator")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeFranceRow.Companion")))
@interface InseeutilsLifeFranceRowCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeSection")))
@interface InseeutilsLifeSection : InseeutilsBase
- (instancetype)initWithLife_europe_card:(InseeutilsLifeEuropeCard *)life_europe_card life_france_card:(InseeutilsLifeFranceCard *)life_france_card section_title:(NSString *)section_title __attribute__((swift_name("init(life_europe_card:life_france_card:section_title:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLifeEuropeCard *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsLifeFranceCard *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsLifeSection *)doCopyLife_europe_card:(InseeutilsLifeEuropeCard *)life_europe_card life_france_card:(InseeutilsLifeFranceCard *)life_france_card section_title:(NSString *)section_title __attribute__((swift_name("doCopy(life_europe_card:life_france_card:section_title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLifeEuropeCard *life_europe_card __attribute__((swift_name("life_europe_card")));
@property (readonly) InseeutilsLifeFranceCard *life_france_card __attribute__((swift_name("life_france_card")));
@property (readonly) NSString *section_title __attribute__((swift_name("section_title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LifeSection.Companion")))
@interface InseeutilsLifeSectionCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Reference")))
@interface InseeutilsReference : InseeutilsBase
- (instancetype)initWithReference_links:(NSArray<InseeutilsReferenceLink *> *)reference_links title:(NSString *)title __attribute__((swift_name("init(reference_links:title:)"))) __attribute__((objc_designated_initializer));
- (NSArray<InseeutilsReferenceLink *> *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsReference *)doCopyReference_links:(NSArray<InseeutilsReferenceLink *> *)reference_links title:(NSString *)title __attribute__((swift_name("doCopy(reference_links:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<InseeutilsReferenceLink *> *reference_links __attribute__((swift_name("reference_links")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Reference.Companion")))
@interface InseeutilsReferenceCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReferenceLink")))
@interface InseeutilsReferenceLink : InseeutilsBase
- (instancetype)initWithTitle:(NSString *)title url:(NSString *)url __attribute__((swift_name("init(title:url:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsReferenceLink *)doCopyTitle:(NSString *)title url:(NSString *)url __attribute__((swift_name("doCopy(title:url:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReferenceLink.Companion")))
@interface InseeutilsReferenceLinkCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Sexe")))
@interface InseeutilsSexe : InseeutilsKotlinEnum<InseeutilsSexe *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) InseeutilsSexe *female __attribute__((swift_name("female")));
@property (class, readonly) InseeutilsSexe *male __attribute__((swift_name("male")));
+ (InseeutilsKotlinArray<InseeutilsSexe *> *)values __attribute__((swift_name("values()")));
@property (readonly) int32_t sexeValue __attribute__((swift_name("sexeValue")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingAgeCard")))
@interface InseeutilsWeddingAgeCard : InseeutilsBase
- (instancetype)initWithHeader:(NSString *)header wedding_rows:(NSArray<InseeutilsWeddingRow *> *)wedding_rows __attribute__((swift_name("init(header:wedding_rows:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSArray<InseeutilsWeddingRow *> *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsWeddingAgeCard *)doCopyHeader:(NSString *)header wedding_rows:(NSArray<InseeutilsWeddingRow *> *)wedding_rows __attribute__((swift_name("doCopy(header:wedding_rows:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@property (readonly) NSArray<InseeutilsWeddingRow *> *wedding_rows __attribute__((swift_name("wedding_rows")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingAgeCard.Companion")))
@interface InseeutilsWeddingAgeCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingRow")))
@interface InseeutilsWeddingRow : InseeutilsBase
- (instancetype)initWithFemale:(NSString *)female male:(NSString *)male separator:(NSString *)separator __attribute__((swift_name("init(female:male:separator:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsWeddingRow *)doCopyFemale:(NSString *)female male:(NSString *)male separator:(NSString *)separator __attribute__((swift_name("doCopy(female:male:separator:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *female __attribute__((swift_name("female")));
@property (readonly) NSString *male __attribute__((swift_name("male")));
@property (readonly) NSString *separator __attribute__((swift_name("separator")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingRow.Companion")))
@interface InseeutilsWeddingRowCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingSection")))
@interface InseeutilsWeddingSection : InseeutilsBase
- (instancetype)initWithSection_title:(NSString *)section_title wedding_age_card:(InseeutilsWeddingAgeCard *)wedding_age_card wedding_thirty_card:(InseeutilsWeddingThirtyCard *)wedding_thirty_card __attribute__((swift_name("init(section_title:wedding_age_card:wedding_thirty_card:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsWeddingAgeCard *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsWeddingThirtyCard *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsWeddingSection *)doCopySection_title:(NSString *)section_title wedding_age_card:(InseeutilsWeddingAgeCard *)wedding_age_card wedding_thirty_card:(InseeutilsWeddingThirtyCard *)wedding_thirty_card __attribute__((swift_name("doCopy(section_title:wedding_age_card:wedding_thirty_card:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *section_title __attribute__((swift_name("section_title")));
@property (readonly) InseeutilsWeddingAgeCard *wedding_age_card __attribute__((swift_name("wedding_age_card")));
@property (readonly) InseeutilsWeddingThirtyCard *wedding_thirty_card __attribute__((swift_name("wedding_thirty_card")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingSection.Companion")))
@interface InseeutilsWeddingSectionCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingThirtyCard")))
@interface InseeutilsWeddingThirtyCard : InseeutilsBase
- (instancetype)initWithHeader:(NSString *)header wedding_rows:(NSArray<InseeutilsWeddingRow *> *)wedding_rows __attribute__((swift_name("init(header:wedding_rows:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSArray<InseeutilsWeddingRow *> *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsWeddingThirtyCard *)doCopyHeader:(NSString *)header wedding_rows:(NSArray<InseeutilsWeddingRow *> *)wedding_rows __attribute__((swift_name("doCopy(header:wedding_rows:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@property (readonly) NSArray<InseeutilsWeddingRow *> *wedding_rows __attribute__((swift_name("wedding_rows")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeddingThirtyCard.Companion")))
@interface InseeutilsWeddingThirtyCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerEnterpriseCard")))
@interface InseeutilsWorkerEnterpriseCard : InseeutilsBase
- (instancetype)initWithFigure:(NSString *)figure legend:(NSString *)legend separator:(NSString *)separator subtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(figure:legend:separator:subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (InseeutilsWorkerEnterpriseCard *)doCopyFigure:(NSString *)figure legend:(NSString *)legend separator:(NSString *)separator subtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(figure:legend:separator:subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *figure __attribute__((swift_name("figure")));
@property (readonly) NSString *legend __attribute__((swift_name("legend")));
@property (readonly) NSString *separator __attribute__((swift_name("separator")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerEnterpriseCard.Companion")))
@interface InseeutilsWorkerEnterpriseCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenderCard")))
@interface InseeutilsWorkerGenderCard : InseeutilsBase
- (instancetype)initWithFooter:(NSString *)footer header:(NSString *)header title:(NSString *)title __attribute__((swift_name("init(footer:header:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsWorkerGenderCard *)doCopyFooter:(NSString *)footer header:(NSString *)header title:(NSString *)title __attribute__((swift_name("doCopy(footer:header:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *footer __attribute__((swift_name("footer")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenderCard.Companion")))
@interface InseeutilsWorkerGenderCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenerationBefore")))
@interface InseeutilsWorkerGenerationBefore : InseeutilsBase
- (instancetype)initWithSubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsWorkerGenerationBefore *)doCopySubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenerationBefore.Companion")))
@interface InseeutilsWorkerGenerationBeforeCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenerationCard")))
@interface InseeutilsWorkerGenerationCard : InseeutilsBase
- (instancetype)initWithHeader:(NSString *)header worker_generation_before:(InseeutilsWorkerGenerationBefore *)worker_generation_before worker_generation_now:(InseeutilsWorkerGenerationNow *)worker_generation_now __attribute__((swift_name("init(header:worker_generation_before:worker_generation_now:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsWorkerGenerationBefore *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsWorkerGenerationNow *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsWorkerGenerationCard *)doCopyHeader:(NSString *)header worker_generation_before:(InseeutilsWorkerGenerationBefore *)worker_generation_before worker_generation_now:(InseeutilsWorkerGenerationNow *)worker_generation_now __attribute__((swift_name("doCopy(header:worker_generation_before:worker_generation_now:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *header __attribute__((swift_name("header")));
@property (readonly) InseeutilsWorkerGenerationBefore *worker_generation_before __attribute__((swift_name("worker_generation_before")));
@property (readonly) InseeutilsWorkerGenerationNow *worker_generation_now __attribute__((swift_name("worker_generation_now")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenerationCard.Companion")))
@interface InseeutilsWorkerGenerationCardCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenerationNow")))
@interface InseeutilsWorkerGenerationNow : InseeutilsBase
- (instancetype)initWithSubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("init(subtitle:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsWorkerGenerationNow *)doCopySubtitle:(NSString *)subtitle title:(NSString *)title __attribute__((swift_name("doCopy(subtitle:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *subtitle __attribute__((swift_name("subtitle")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerGenerationNow.Companion")))
@interface InseeutilsWorkerGenerationNowCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerSection")))
@interface InseeutilsWorkerSection : InseeutilsBase
- (instancetype)initWithSection_title:(NSString *)section_title worker_enterprise_card:(InseeutilsWorkerEnterpriseCard *)worker_enterprise_card worker_gender_card:(InseeutilsWorkerGenderCard *)worker_gender_card worker_generation_card:(InseeutilsWorkerGenerationCard *)worker_generation_card __attribute__((swift_name("init(section_title:worker_enterprise_card:worker_gender_card:worker_generation_card:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsWorkerEnterpriseCard *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsWorkerGenderCard *)component3 __attribute__((swift_name("component3()")));
- (InseeutilsWorkerGenerationCard *)component4 __attribute__((swift_name("component4()")));
- (InseeutilsWorkerSection *)doCopySection_title:(NSString *)section_title worker_enterprise_card:(InseeutilsWorkerEnterpriseCard *)worker_enterprise_card worker_gender_card:(InseeutilsWorkerGenderCard *)worker_gender_card worker_generation_card:(InseeutilsWorkerGenerationCard *)worker_generation_card __attribute__((swift_name("doCopy(section_title:worker_enterprise_card:worker_gender_card:worker_generation_card:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *section_title __attribute__((swift_name("section_title")));
@property (readonly) InseeutilsWorkerEnterpriseCard *worker_enterprise_card __attribute__((swift_name("worker_enterprise_card")));
@property (readonly) InseeutilsWorkerGenderCard *worker_gender_card __attribute__((swift_name("worker_gender_card")));
@property (readonly) InseeutilsWorkerGenerationCard *worker_generation_card __attribute__((swift_name("worker_generation_card")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WorkerSection.Companion")))
@interface InseeutilsWorkerSectionCompanion : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Thematic")))
@interface InseeutilsThematic : InseeutilsBase
- (instancetype)initWithName:(NSString *)name reference:(NSString *)reference title:(int32_t)title bigTitle:(int32_t)bigTitle description:(int32_t)description color1:(int32_t)color1 color2:(int32_t)color2 color3:(int32_t)color3 color4:(int32_t)color4 color5:(int32_t)color5 imageTheme:(int32_t)imageTheme imageIndicator:(int32_t)imageIndicator imageTopDidYouKnown:(int32_t)imageTopDidYouKnown imageBottomDidYouKnown:(int32_t)imageBottomDidYouKnown imageTrueFalseCorner:(int32_t)imageTrueFalseCorner imageTrueFalseRightAnswer:(int32_t)imageTrueFalseRightAnswer imageQuizCornerTopRight:(int32_t)imageQuizCornerTopRight imageQuizCornerBottomLeft:(int32_t)imageQuizCornerBottomLeft imageTopBanner:(int32_t)imageTopBanner imageQuizProgression:(int32_t)imageQuizProgression imageRightAnswer:(int32_t)imageRightAnswer imageWrongAnswer:(int32_t)imageWrongAnswer imageCloseQuiz:(int32_t)imageCloseQuiz __attribute__((swift_name("init(name:reference:title:bigTitle:description:color1:color2:color3:color4:color5:imageTheme:imageIndicator:imageTopDidYouKnown:imageBottomDidYouKnown:imageTrueFalseCorner:imageTrueFalseRightAnswer:imageQuizCornerTopRight:imageQuizCornerBottomLeft:imageTopBanner:imageQuizProgression:imageRightAnswer:imageWrongAnswer:imageCloseQuiz:)"))) __attribute__((objc_designated_initializer));
@property int32_t bigTitle __attribute__((swift_name("bigTitle")));
@property int32_t color1 __attribute__((swift_name("color1")));
@property int32_t color2 __attribute__((swift_name("color2")));
@property int32_t color3 __attribute__((swift_name("color3")));
@property int32_t color4 __attribute__((swift_name("color4")));
@property int32_t color5 __attribute__((swift_name("color5")));
@property (setter=setDescription:) int32_t description_ __attribute__((swift_name("description_")));
@property int32_t imageBottomDidYouKnown __attribute__((swift_name("imageBottomDidYouKnown")));
@property int32_t imageCloseQuiz __attribute__((swift_name("imageCloseQuiz")));
@property int32_t imageIndicator __attribute__((swift_name("imageIndicator")));
@property int32_t imageQuizCornerBottomLeft __attribute__((swift_name("imageQuizCornerBottomLeft")));
@property int32_t imageQuizCornerTopRight __attribute__((swift_name("imageQuizCornerTopRight")));
@property int32_t imageQuizProgression __attribute__((swift_name("imageQuizProgression")));
@property int32_t imageRightAnswer __attribute__((swift_name("imageRightAnswer")));
@property int32_t imageTheme __attribute__((swift_name("imageTheme")));
@property int32_t imageTopBanner __attribute__((swift_name("imageTopBanner")));
@property int32_t imageTopDidYouKnown __attribute__((swift_name("imageTopDidYouKnown")));
@property int32_t imageTrueFalseCorner __attribute__((swift_name("imageTrueFalseCorner")));
@property int32_t imageTrueFalseRightAnswer __attribute__((swift_name("imageTrueFalseRightAnswer")));
@property int32_t imageWrongAnswer __attribute__((swift_name("imageWrongAnswer")));
@property NSString *name __attribute__((swift_name("name")));
@property NSString *reference __attribute__((swift_name("reference")));
@property int32_t title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ThematicDb")))
@interface InseeutilsThematicDb : InseeutilsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)thematicDb __attribute__((swift_name("init()")));
- (InseeutilsThematic *)findRef:(NSString * _Nullable)ref __attribute__((swift_name("find(ref:)")));
- (InseeutilsThematic *)getDefault __attribute__((swift_name("getDefault()")));
- (NSString *)getDefaultRef __attribute__((swift_name("getDefaultRef()")));
- (NSString *)getEcologyRef __attribute__((swift_name("getEcologyRef()")));
- (NSString *)getEconomyRef __attribute__((swift_name("getEconomyRef()")));
- (NSString *)getJobRef __attribute__((swift_name("getJobRef()")));
- (NSString *)getLifeStyleRef __attribute__((swift_name("getLifeStyleRef()")));
- (NSString *)getPopulationRef __attribute__((swift_name("getPopulationRef()")));
@property (readonly) NSArray<InseeutilsThematic *> *themeList __attribute__((swift_name("themeList")));
@end;

__attribute__((swift_name("ConvertiseurModelQueries")))
@protocol InseeutilsConvertiseurModelQueries <InseeutilsRuntimeTransacter>
@required
- (void)insertItemAnnee:(int64_t)annee valeur:(float)valeur __attribute__((swift_name("insertItem(annee:valeur:)")));
- (InseeutilsRuntimeQuery<InseeutilsConvertisseur *> *)selectAll __attribute__((swift_name("selectAll()")));
- (InseeutilsRuntimeQuery<id> *)selectAllMapper:(id (^)(InseeutilsLong *, InseeutilsLong *, InseeutilsFloat *))mapper __attribute__((swift_name("selectAll(mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsSelectMax *> *)selectMax __attribute__((swift_name("selectMax()")));
- (InseeutilsRuntimeQuery<id> *)selectMaxMapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("selectMax(mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsSelectMin *> *)selectMin __attribute__((swift_name("selectMin()")));
- (InseeutilsRuntimeQuery<id> *)selectMinMapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("selectMin(mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsFloat *> *)selectYearAnnee:(int64_t)annee __attribute__((swift_name("selectYear(annee:)")));
- (InseeutilsRuntimeQuery<InseeutilsLong *> *)selectYears __attribute__((swift_name("selectYears()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Convertisseur")))
@interface InseeutilsConvertisseur : InseeutilsBase
- (instancetype)initWithId:(int64_t)id annee:(int64_t)annee valeur:(float)valeur __attribute__((swift_name("init(id:annee:valeur:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (float)component3 __attribute__((swift_name("component3()")));
- (InseeutilsConvertisseur *)doCopyId:(int64_t)id annee:(int64_t)annee valeur:(float)valeur __attribute__((swift_name("doCopy(id:annee:valeur:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t annee __attribute__((swift_name("annee")));
@property (readonly) int64_t id __attribute__((swift_name("id")));
@property (readonly) float valeur __attribute__((swift_name("valeur")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetAccuranceNumberOnYearRangeById")))
@interface InseeutilsGetAccuranceNumberOnYearRangeById : InseeutilsBase
- (instancetype)initWithCnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("init(cnt:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsGetAccuranceNumberOnYearRangeById *)doCopyCnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("doCopy(cnt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable cnt __attribute__((swift_name("cnt")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetBestRankInYearRange")))
@interface InseeutilsGetBestRankInYearRange : InseeutilsBase
- (instancetype)initWithPrenom:(NSString *)prenom cnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("init(prenom:cnt:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (InseeutilsLong * _Nullable)component2 __attribute__((swift_name("component2()")));
- (InseeutilsGetBestRankInYearRange *)doCopyPrenom:(NSString *)prenom cnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("doCopy(prenom:cnt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable cnt __attribute__((swift_name("cnt")));
@property (readonly) NSString *prenom __attribute__((swift_name("prenom")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetBestYearsRank")))
@interface InseeutilsGetBestYearsRank : InseeutilsBase
- (instancetype)initWithAnnee:(int64_t)annee rank:(int64_t)rank __attribute__((swift_name("init(annee:rank:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (InseeutilsGetBestYearsRank *)doCopyAnnee:(int64_t)annee rank:(int64_t)rank __attribute__((swift_name("doCopy(annee:rank:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t annee __attribute__((swift_name("annee")));
@property (readonly) int64_t rank __attribute__((swift_name("rank")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetMaxFirstNameYear")))
@interface InseeutilsGetMaxFirstNameYear : InseeutilsBase
- (instancetype)initWithMAX:(InseeutilsLong * _Nullable)MAX __attribute__((swift_name("init(MAX:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsGetMaxFirstNameYear *)doCopyMAX:(InseeutilsLong * _Nullable)MAX __attribute__((swift_name("doCopy(MAX:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable MAX __attribute__((swift_name("MAX")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetMaxRentYear")))
@interface InseeutilsGetMaxRentYear : InseeutilsBase
- (instancetype)initWithMAX:(InseeutilsLong * _Nullable)MAX __attribute__((swift_name("init(MAX:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsGetMaxRentYear *)doCopyMAX:(InseeutilsLong * _Nullable)MAX __attribute__((swift_name("doCopy(MAX:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable MAX __attribute__((swift_name("MAX")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetMinFirstNameYear")))
@interface InseeutilsGetMinFirstNameYear : InseeutilsBase
- (instancetype)initWithMIN:(InseeutilsLong * _Nullable)MIN __attribute__((swift_name("init(MIN:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsGetMinFirstNameYear *)doCopyMIN:(InseeutilsLong * _Nullable)MIN __attribute__((swift_name("doCopy(MIN:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable MIN __attribute__((swift_name("MIN")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetMinRentYear")))
@interface InseeutilsGetMinRentYear : InseeutilsBase
- (instancetype)initWithMIN:(InseeutilsLong * _Nullable)MIN __attribute__((swift_name("init(MIN:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsGetMinRentYear *)doCopyMIN:(InseeutilsLong * _Nullable)MIN __attribute__((swift_name("doCopy(MIN:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable MIN __attribute__((swift_name("MIN")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetRankByCount")))
@interface InseeutilsGetRankByCount : InseeutilsBase
- (instancetype)initWithCnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("init(cnt:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsGetRankByCount *)doCopyCnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("doCopy(cnt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable cnt __attribute__((swift_name("cnt")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetRentUpdate")))
@interface InseeutilsGetRentUpdate : InseeutilsBase
- (instancetype)initWithAnnee:(int64_t)annee trimestre:(NSString *)trimestre n:(double)n p:(double)p loyer:(double)loyer __attribute__((swift_name("init(annee:trimestre:n:p:loyer:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (double)component3 __attribute__((swift_name("component3()")));
- (double)component4 __attribute__((swift_name("component4()")));
- (double)component5 __attribute__((swift_name("component5()")));
- (InseeutilsGetRentUpdate *)doCopyAnnee:(int64_t)annee trimestre:(NSString *)trimestre n:(double)n p:(double)p loyer:(double)loyer __attribute__((swift_name("doCopy(annee:trimestre:n:p:loyer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t annee __attribute__((swift_name("annee")));
@property (readonly) double loyer __attribute__((swift_name("loyer")));
@property (readonly) double n __attribute__((swift_name("n")));
@property (readonly) double p __attribute__((swift_name("p")));
@property (readonly) NSString *trimestre __attribute__((swift_name("trimestre")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Loyer")))
@interface InseeutilsLoyer : InseeutilsBase
- (instancetype)initWithId:(int64_t)id annee:(int64_t)annee trimestre:(NSString *)trimestre valeur:(double)valeur __attribute__((swift_name("init(id:annee:trimestre:valeur:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (double)component4 __attribute__((swift_name("component4()")));
- (InseeutilsLoyer *)doCopyId:(int64_t)id annee:(int64_t)annee trimestre:(NSString *)trimestre valeur:(double)valeur __attribute__((swift_name("doCopy(id:annee:trimestre:valeur:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t annee __attribute__((swift_name("annee")));
@property (readonly) int64_t id __attribute__((swift_name("id")));
@property (readonly) NSString *trimestre __attribute__((swift_name("trimestre")));
@property (readonly) double valeur __attribute__((swift_name("valeur")));
@end;

__attribute__((swift_name("NamesModelQueries")))
@protocol InseeutilsNamesModelQueries <InseeutilsRuntimeTransacter>
@required
- (InseeutilsRuntimeQuery<InseeutilsGetAccuranceNumberOnYearRangeById *> *)getAccuranceNumberOnYearRangeByIdFromYear:(int64_t)fromYear toYear:(int64_t)toYear prenomid:(int64_t)prenomid __attribute__((swift_name("getAccuranceNumberOnYearRangeById(fromYear:toYear:prenomid:)")));
- (InseeutilsRuntimeQuery<id> *)getAccuranceNumberOnYearRangeByIdFromYear:(int64_t)fromYear toYear:(int64_t)toYear prenomid:(int64_t)prenomid mapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("getAccuranceNumberOnYearRangeById(fromYear:toYear:prenomid:mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetBestRankInYearRange *> *)getBestRankInYearRangeFromYear:(int64_t)fromYear toYear:(int64_t)toYear sexe:(InseeutilsSexe *)sexe __attribute__((swift_name("getBestRankInYearRange(fromYear:toYear:sexe:)")));
- (InseeutilsRuntimeQuery<id> *)getBestRankInYearRangeFromYear:(int64_t)fromYear toYear:(int64_t)toYear sexe:(InseeutilsSexe *)sexe mapper:(id (^)(NSString *, InseeutilsLong * _Nullable))mapper __attribute__((swift_name("getBestRankInYearRange(fromYear:toYear:sexe:mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetBestYearsRank *> *)getBestYearsRankId:(int64_t)id __attribute__((swift_name("getBestYearsRank(id:)")));
- (InseeutilsRuntimeQuery<id> *)getBestYearsRankId:(int64_t)id mapper:(id (^)(InseeutilsLong *, InseeutilsLong *))mapper __attribute__((swift_name("getBestYearsRank(id:mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetMaxFirstNameYear *> *)getMaxFirstNameYear __attribute__((swift_name("getMaxFirstNameYear()")));
- (InseeutilsRuntimeQuery<id> *)getMaxFirstNameYearMapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("getMaxFirstNameYear(mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetMinFirstNameYear *> *)getMinFirstNameYear __attribute__((swift_name("getMinFirstNameYear()")));
- (InseeutilsRuntimeQuery<id> *)getMinFirstNameYearMapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("getMinFirstNameYear(mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsPrenoms *> *)getPrenomByIdPrenomid:(int64_t)prenomid __attribute__((swift_name("getPrenomById(prenomid:)")));
- (InseeutilsRuntimeQuery<id> *)getPrenomByIdPrenomid:(int64_t)prenomid mapper:(id (^)(InseeutilsLong *, NSString *, InseeutilsSexe *, InseeutilsLong *))mapper __attribute__((swift_name("getPrenomById(prenomid:mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetRankByCount *> *)getRankByCountFromYear:(int64_t)fromYear toYear:(int64_t)toYear sexe:(InseeutilsSexe *)sexe __attribute__((swift_name("getRankByCount(fromYear:toYear:sexe:)")));
- (InseeutilsRuntimeQuery<id> *)getRankByCountFromYear:(int64_t)fromYear toYear:(int64_t)toYear sexe:(InseeutilsSexe *)sexe mapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("getRankByCount(fromYear:toYear:sexe:mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsLong *> *)getYearList __attribute__((swift_name("getYearList()")));
- (InseeutilsRuntimeQuery<InseeutilsPrenoms *> *)searchForFirstnamePrenom:(NSString *)prenom __attribute__((swift_name("searchForFirstname(prenom:)")));
- (InseeutilsRuntimeQuery<id> *)searchForFirstnamePrenom:(NSString *)prenom mapper:(id (^)(InseeutilsLong *, NSString *, InseeutilsSexe *, InseeutilsLong *))mapper __attribute__((swift_name("searchForFirstname(prenom:mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsTop100FirstNames *> *)top100FirstNamesSexe:(InseeutilsSexe *)sexe annee:(int64_t)annee annee_:(int64_t)annee_ __attribute__((swift_name("top100FirstNames(sexe:annee:annee_:)")));
- (InseeutilsRuntimeQuery<id> *)top100FirstNamesSexe:(InseeutilsSexe *)sexe annee:(int64_t)annee annee_:(int64_t)annee_ mapper:(id (^)(InseeutilsLong *, NSString *, InseeutilsSexe *, InseeutilsLong *, InseeutilsLong * _Nullable))mapper __attribute__((swift_name("top100FirstNames(sexe:annee:annee_:mapper:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Prenoms")))
@interface InseeutilsPrenoms : InseeutilsBase
- (instancetype)initWithId:(int64_t)id prenom:(NSString *)prenom sexe:(InseeutilsSexe *)sexe mixte:(int64_t)mixte __attribute__((swift_name("init(id:prenom:sexe:mixte:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsSexe *)component3 __attribute__((swift_name("component3()")));
- (int64_t)component4 __attribute__((swift_name("component4()")));
- (InseeutilsPrenoms *)doCopyId:(int64_t)id prenom:(NSString *)prenom sexe:(InseeutilsSexe *)sexe mixte:(int64_t)mixte __attribute__((swift_name("doCopy(id:prenom:sexe:mixte:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t id __attribute__((swift_name("id")));
@property (readonly) int64_t mixte __attribute__((swift_name("mixte")));
@property (readonly) NSString *prenom __attribute__((swift_name("prenom")));
@property (readonly) InseeutilsSexe *sexe __attribute__((swift_name("sexe")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Prenoms.Adapter")))
@interface InseeutilsPrenomsAdapter : InseeutilsBase
- (instancetype)initWithSexeAdapter:(id<InseeutilsRuntimeColumnAdapter>)sexeAdapter __attribute__((swift_name("init(sexeAdapter:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<InseeutilsRuntimeColumnAdapter> sexeAdapter __attribute__((swift_name("sexeAdapter")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Prenoms_stats")))
@interface InseeutilsPrenoms_stats : InseeutilsBase
- (instancetype)initWithId:(int64_t)id prenom_id:(int64_t)prenom_id annee:(int64_t)annee nombre:(int64_t)nombre rank:(int64_t)rank __attribute__((swift_name("init(id:prenom_id:annee:nombre:rank:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (int64_t)component3 __attribute__((swift_name("component3()")));
- (int64_t)component4 __attribute__((swift_name("component4()")));
- (int64_t)component5 __attribute__((swift_name("component5()")));
- (InseeutilsPrenoms_stats *)doCopyId:(int64_t)id prenom_id:(int64_t)prenom_id annee:(int64_t)annee nombre:(int64_t)nombre rank:(int64_t)rank __attribute__((swift_name("doCopy(id:prenom_id:annee:nombre:rank:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t annee __attribute__((swift_name("annee")));
@property (readonly) int64_t id __attribute__((swift_name("id")));
@property (readonly) int64_t nombre __attribute__((swift_name("nombre")));
@property (readonly) int64_t prenom_id __attribute__((swift_name("prenom_id")));
@property (readonly) int64_t rank __attribute__((swift_name("rank")));
@end;

__attribute__((swift_name("RentModelQueries")))
@protocol InseeutilsRentModelQueries <InseeutilsRuntimeTransacter>
@required
- (InseeutilsRuntimeQuery<InseeutilsGetMaxRentYear *> *)getMaxRentYear __attribute__((swift_name("getMaxRentYear()")));
- (InseeutilsRuntimeQuery<id> *)getMaxRentYearMapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("getMaxRentYear(mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetMinRentYear *> *)getMinRentYear __attribute__((swift_name("getMinRentYear()")));
- (InseeutilsRuntimeQuery<id> *)getMinRentYearMapper:(id (^)(InseeutilsLong * _Nullable))mapper __attribute__((swift_name("getMinRentYear(mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsGetRentUpdate *> *)getRentUpdateRent:(double)rent trimester:(NSString *)trimester bailDate:(NSString *)bailDate __attribute__((swift_name("getRentUpdate(rent:trimester:bailDate:)")));
- (InseeutilsRuntimeQuery<id> *)getRentUpdateRent:(double)rent trimester:(NSString *)trimester bailDate:(NSString *)bailDate mapper:(id (^)(InseeutilsLong *, NSString *, InseeutilsDouble *, InseeutilsDouble *, InseeutilsDouble *))mapper __attribute__((swift_name("getRentUpdate(rent:trimester:bailDate:mapper:)")));
- (InseeutilsRuntimeQuery<InseeutilsLong *> *)getYearList __attribute__((swift_name("getYearList()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SelectMax")))
@interface InseeutilsSelectMax : InseeutilsBase
- (instancetype)initWithMAX:(InseeutilsLong * _Nullable)MAX __attribute__((swift_name("init(MAX:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsSelectMax *)doCopyMAX:(InseeutilsLong * _Nullable)MAX __attribute__((swift_name("doCopy(MAX:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable MAX __attribute__((swift_name("MAX")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SelectMin")))
@interface InseeutilsSelectMin : InseeutilsBase
- (instancetype)initWithMIN:(InseeutilsLong * _Nullable)MIN __attribute__((swift_name("init(MIN:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsLong * _Nullable)component1 __attribute__((swift_name("component1()")));
- (InseeutilsSelectMin *)doCopyMIN:(InseeutilsLong * _Nullable)MIN __attribute__((swift_name("doCopy(MIN:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable MIN __attribute__((swift_name("MIN")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Top100FirstNames")))
@interface InseeutilsTop100FirstNames : InseeutilsBase
- (instancetype)initWithId:(int64_t)id prenom:(NSString *)prenom sexe:(InseeutilsSexe *)sexe mixte:(int64_t)mixte cnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("init(id:prenom:sexe:mixte:cnt:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (InseeutilsSexe *)component3 __attribute__((swift_name("component3()")));
- (int64_t)component4 __attribute__((swift_name("component4()")));
- (InseeutilsLong * _Nullable)component5 __attribute__((swift_name("component5()")));
- (InseeutilsTop100FirstNames *)doCopyId:(int64_t)id prenom:(NSString *)prenom sexe:(InseeutilsSexe *)sexe mixte:(int64_t)mixte cnt:(InseeutilsLong * _Nullable)cnt __attribute__((swift_name("doCopy(id:prenom:sexe:mixte:cnt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsLong * _Nullable cnt __attribute__((swift_name("cnt")));
@property (readonly) int64_t id __attribute__((swift_name("id")));
@property (readonly) int64_t mixte __attribute__((swift_name("mixte")));
@property (readonly) NSString *prenom __attribute__((swift_name("prenom")));
@property (readonly) InseeutilsSexe *sexe __attribute__((swift_name("sexe")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ExtensionsKt")))
@interface InseeutilsExtensionsKt : InseeutilsBase
+ (float)round:(float)receiver decimals:(int32_t)decimals __attribute__((swift_name("round(_:decimals:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ColumnAdaptersKt")))
@interface InseeutilsColumnAdaptersKt : InseeutilsBase
@property (class, readonly) id<InseeutilsRuntimeColumnAdapter> SexeAdapter __attribute__((swift_name("SexeAdapter")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface InseeutilsKotlinThrowable : InseeutilsBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(InseeutilsKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(InseeutilsKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (InseeutilsKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) InseeutilsKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((swift_name("KotlinError")))
@interface InseeutilsKotlinError : InseeutilsKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(InseeutilsKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(InseeutilsKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface InseeutilsKotlinArray<T> : InseeutilsBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(InseeutilsInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<InseeutilsKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol InseeutilsKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<InseeutilsKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<InseeutilsKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol InseeutilsKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<InseeutilsKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<InseeutilsKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol InseeutilsKotlinx_serialization_coreKSerializer <InseeutilsKotlinx_serialization_coreSerializationStrategy, InseeutilsKotlinx_serialization_coreDeserializationStrategy>
@required
@end;

__attribute__((swift_name("RuntimeTransactionCallbacks")))
@protocol InseeutilsRuntimeTransactionCallbacks
@required
- (void)afterCommitFunction:(void (^)(void))function __attribute__((swift_name("afterCommit(function:)")));
- (void)afterRollbackFunction:(void (^)(void))function __attribute__((swift_name("afterRollback(function:)")));
@end;

__attribute__((swift_name("RuntimeTransactionWithoutReturn")))
@protocol InseeutilsRuntimeTransactionWithoutReturn <InseeutilsRuntimeTransactionCallbacks>
@required
- (void)rollback __attribute__((swift_name("rollback()")));
- (void)transactionBody:(void (^)(id<InseeutilsRuntimeTransactionWithoutReturn>))body __attribute__((swift_name("transaction(body:)")));
@end;

__attribute__((swift_name("RuntimeTransactionWithReturn")))
@protocol InseeutilsRuntimeTransactionWithReturn <InseeutilsRuntimeTransactionCallbacks>
@required
- (void)rollbackReturnValue:(id _Nullable)returnValue __attribute__((swift_name("rollback(returnValue:)")));
- (id _Nullable)transactionBody_:(id _Nullable (^)(id<InseeutilsRuntimeTransactionWithReturn>))body __attribute__((swift_name("transaction(body_:)")));
@end;

__attribute__((swift_name("RuntimeCloseable")))
@protocol InseeutilsRuntimeCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end;

__attribute__((swift_name("RuntimeSqlDriver")))
@protocol InseeutilsRuntimeSqlDriver <InseeutilsRuntimeCloseable>
@required
- (InseeutilsRuntimeTransacterTransaction * _Nullable)currentTransaction __attribute__((swift_name("currentTransaction()")));
- (void)executeIdentifier:(InseeutilsInt * _Nullable)identifier sql:(NSString *)sql parameters:(int32_t)parameters binders:(void (^ _Nullable)(id<InseeutilsRuntimeSqlPreparedStatement>))binders __attribute__((swift_name("execute(identifier:sql:parameters:binders:)")));
- (id<InseeutilsRuntimeSqlCursor>)executeQueryIdentifier:(InseeutilsInt * _Nullable)identifier sql:(NSString *)sql parameters:(int32_t)parameters binders:(void (^ _Nullable)(id<InseeutilsRuntimeSqlPreparedStatement>))binders __attribute__((swift_name("executeQuery(identifier:sql:parameters:binders:)")));
- (InseeutilsRuntimeTransacterTransaction *)doNewTransaction __attribute__((swift_name("doNewTransaction()")));
@end;

__attribute__((swift_name("RuntimeSqlDriverSchema")))
@protocol InseeutilsRuntimeSqlDriverSchema
@required
- (void)createDriver:(id<InseeutilsRuntimeSqlDriver>)driver __attribute__((swift_name("create(driver:)")));
- (void)migrateDriver:(id<InseeutilsRuntimeSqlDriver>)driver oldVersion:(int32_t)oldVersion newVersion:(int32_t)newVersion __attribute__((swift_name("migrate(driver:oldVersion:newVersion:)")));
@property (readonly) int32_t version __attribute__((swift_name("version")));
@end;

__attribute__((swift_name("RuntimeQuery")))
@interface InseeutilsRuntimeQuery<__covariant RowType> : InseeutilsBase
- (instancetype)initWithQueries:(NSMutableArray<InseeutilsRuntimeQuery<id> *> *)queries mapper:(RowType (^)(id<InseeutilsRuntimeSqlCursor>))mapper __attribute__((swift_name("init(queries:mapper:)"))) __attribute__((objc_designated_initializer));
- (void)addListenerListener:(id<InseeutilsRuntimeQueryListener>)listener __attribute__((swift_name("addListener(listener:)")));
- (id<InseeutilsRuntimeSqlCursor>)execute __attribute__((swift_name("execute()")));
- (NSArray<RowType> *)executeAsList __attribute__((swift_name("executeAsList()")));
- (RowType)executeAsOne __attribute__((swift_name("executeAsOne()")));
- (RowType _Nullable)executeAsOneOrNull __attribute__((swift_name("executeAsOneOrNull()")));
- (void)notifyDataChanged __attribute__((swift_name("notifyDataChanged()")));
- (void)removeListenerListener:(id<InseeutilsRuntimeQueryListener>)listener __attribute__((swift_name("removeListener(listener:)")));
@property (readonly) RowType (^mapper)(id<InseeutilsRuntimeSqlCursor>) __attribute__((swift_name("mapper")));
@end;

__attribute__((swift_name("RuntimeColumnAdapter")))
@protocol InseeutilsRuntimeColumnAdapter
@required
- (id)decodeDatabaseValue:(id _Nullable)databaseValue __attribute__((swift_name("decode(databaseValue:)")));
- (id _Nullable)encodeValue:(id)value __attribute__((swift_name("encode(value:)")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol InseeutilsKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol InseeutilsKotlinx_serialization_coreEncoder
@required
- (id<InseeutilsKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<InseeutilsKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<InseeutilsKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<InseeutilsKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) InseeutilsKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol InseeutilsKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<InseeutilsKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<InseeutilsKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) InseeutilsKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol InseeutilsKotlinx_serialization_coreDecoder
@required
- (id<InseeutilsKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (InseeutilsKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<InseeutilsKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<InseeutilsKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) InseeutilsKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("RuntimeTransacterTransaction")))
@interface InseeutilsRuntimeTransacterTransaction : InseeutilsBase <InseeutilsRuntimeTransactionCallbacks>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)afterCommitFunction:(void (^)(void))function __attribute__((swift_name("afterCommit(function:)")));
- (void)afterRollbackFunction:(void (^)(void))function __attribute__((swift_name("afterRollback(function:)")));
- (void)endTransactionSuccessful:(BOOL)successful __attribute__((swift_name("endTransaction(successful:)")));
@property (readonly) InseeutilsRuntimeTransacterTransaction * _Nullable enclosingTransaction __attribute__((swift_name("enclosingTransaction")));
@end;

__attribute__((swift_name("RuntimeSqlPreparedStatement")))
@protocol InseeutilsRuntimeSqlPreparedStatement
@required
- (void)bindBytesIndex:(int32_t)index bytes:(InseeutilsKotlinByteArray * _Nullable)bytes __attribute__((swift_name("bindBytes(index:bytes:)")));
- (void)bindDoubleIndex:(int32_t)index double:(InseeutilsDouble * _Nullable)double_ __attribute__((swift_name("bindDouble(index:double:)")));
- (void)bindLongIndex:(int32_t)index long:(InseeutilsLong * _Nullable)long_ __attribute__((swift_name("bindLong(index:long:)")));
- (void)bindStringIndex:(int32_t)index string:(NSString * _Nullable)string __attribute__((swift_name("bindString(index:string:)")));
@end;

__attribute__((swift_name("RuntimeSqlCursor")))
@protocol InseeutilsRuntimeSqlCursor <InseeutilsRuntimeCloseable>
@required
- (InseeutilsKotlinByteArray * _Nullable)getBytesIndex:(int32_t)index __attribute__((swift_name("getBytes(index:)")));
- (InseeutilsDouble * _Nullable)getDoubleIndex:(int32_t)index __attribute__((swift_name("getDouble(index:)")));
- (InseeutilsLong * _Nullable)getLongIndex:(int32_t)index __attribute__((swift_name("getLong(index:)")));
- (NSString * _Nullable)getStringIndex:(int32_t)index __attribute__((swift_name("getString(index:)")));
- (BOOL)next_ __attribute__((swift_name("next_()")));
@end;

__attribute__((swift_name("RuntimeQueryListener")))
@protocol InseeutilsRuntimeQueryListener
@required
- (void)queryResultsChanged __attribute__((swift_name("queryResultsChanged()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol InseeutilsKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (void)encodeIntElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<InseeutilsKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<InseeutilsKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) InseeutilsKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface InseeutilsKotlinx_serialization_coreSerializersModule : InseeutilsBase
- (void)dumpToCollector:(id<InseeutilsKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<InseeutilsKotlinx_serialization_coreKSerializer> _Nullable)getContextualKclass:(id<InseeutilsKotlinKClass>)kclass __attribute__((swift_name("getContextual(kclass:)")));
- (id<InseeutilsKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<InseeutilsKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<InseeutilsKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<InseeutilsKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol InseeutilsKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface InseeutilsKotlinx_serialization_coreSerialKind : InseeutilsBase
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol InseeutilsKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<InseeutilsKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<InseeutilsKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<InseeutilsKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) InseeutilsKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface InseeutilsKotlinNothing : InseeutilsBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface InseeutilsKotlinByteArray : InseeutilsBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(InseeutilsByte *(^)(InseeutilsInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (InseeutilsKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol InseeutilsKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<InseeutilsKotlinKClass>)kClass serializer:(id<InseeutilsKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<InseeutilsKotlinKClass>)baseClass actualClass:(id<InseeutilsKotlinKClass>)actualClass actualSerializer:(id<InseeutilsKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<InseeutilsKotlinKClass>)baseClass defaultSerializerProvider:(id<InseeutilsKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultSerializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultSerializerProvider:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol InseeutilsKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol InseeutilsKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol InseeutilsKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol InseeutilsKotlinKClass <InseeutilsKotlinKDeclarationContainer, InseeutilsKotlinKAnnotatedElement, InseeutilsKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((swift_name("KotlinByteIterator")))
@interface InseeutilsKotlinByteIterator : InseeutilsBase <InseeutilsKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (InseeutilsByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end;

#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
